package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC_001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC_001_CreateLead";
		testDescription="Login into leafTaps";
		testNodes = "Leads";
		author = "Vaishnavi";
		category="smoke";
		dataSheetName = "TC_001";
		
			
	}
	
	
	@Test(dataProvider="fetchData")
	public void Login(String username, String password, String cname, String fname, String lname)
	{
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password.toLowerCase())
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cname)
		.enterFirstName(fname)
		.enterLastname(lname)
		.clickSubmit()
		.verifyFirstname(fname);
		
	
		
		
		
		/*LoginPage lp = new LoginPage();
		lp.enterUsername(data);
		lp.enterPassword(data);
		lp.clickLogin()*/;
		
	}

}
