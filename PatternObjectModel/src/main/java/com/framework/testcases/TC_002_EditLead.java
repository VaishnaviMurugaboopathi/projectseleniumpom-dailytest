package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.FindLeadPages;
import com.framework.pages.LoginPage;

public class TC_002_EditLead extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC_002_EditLead";
		testDescription="Edit leaftaps";
		testNodes = "Leads";
		author = "Vaishnavi";
		category="smoke";
		dataSheetName = "TC_002";
		
			
	}

	@Test (dataProvider="fetchData")
	public void Editlead(String username, String password, String fname) throws InterruptedException
	
	{
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRM()
		.clickLeads()
		.clickFindleads()
		.enterFirstName(fname)
		.clickFindLeadButton()
		.clickFirstResultingLead();
		
		
	
		
		
		
		
	}
	
}
