package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	public CreateLeadPage enterCompanyName(String data)
	{
		clearAndType(eleCompanyName, data);
		return this;
	}
	
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstname;
	public CreateLeadPage enterFirstName(String data)
	{
		clearAndType(eleFirstname, data);
		return new CreateLeadPage();

	}
	
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastname;
	public CreateLeadPage enterLastname(String data)
	{
		clearAndType(eleLastname, data);
		return new CreateLeadPage();
	}
	
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleSubmit;
	public ViewLead clickSubmit()
	{
		click(eleSubmit);
		return new ViewLead();
	}

}
