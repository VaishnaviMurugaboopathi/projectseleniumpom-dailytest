package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.PageFactoryFinder;

import com.framework.design.ProjectMethods;

public class FindLeadPages extends ProjectMethods {
	
	public FindLeadPages()
	
	{
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]")WebElement eleenterFirstName;
	
	public FindLeadPages enterFirstName(String data)
	{
		clearAndType(eleenterFirstName, data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//em/button[contains(text(),'Find Leads')]") WebElement eleFind;
	
	public FindLeadPages clickFindLeadButton() throws InterruptedException
	{
		click(eleFind);
		Thread.sleep(3000);
		return this;
	
}
	@FindBy (how=How.XPATH, using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]") WebElement eleFirstResultingLead;
	
	public ViewLead clickFirstResultingLead()
	{
		click(eleFirstResultingLead);
		return new ViewLead();
	}
	
}
 
