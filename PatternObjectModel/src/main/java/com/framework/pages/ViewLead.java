package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLead extends ProjectMethods{
	
	public ViewLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="viewLead_firstName_sp") WebElement eleResultingName;
	
	public ViewLead verifyFirstname(String data)
	{
	  verifyExactText(eleResultingName, data);
	  return this;
	}
	

}
